# services

Для работы следует установить

##ansible
Версию не ниже 2.0:

$ sudo apt-get install software-properties-common

$ sudo apt-add-repository ppa:ansible/ansible

$ sudo apt-get update

$ sudo apt-get install ansible


##vagrant
версию не ниже 1.8.1 https://releases.hashicorp.com/vagrant/


## Роли
ansible-galaxy install DavidWittman.redis



# План реализации:

В Vagrantfile делаем только:

1. Создание ВМ;

2. Настройку ip;

3. Для отладки скриптов ansible используем автогенерируемый vagrant_ansible_inventory.


## Как работать:
1. Раскомментировать в /etc/ansible/ansible.cfg строчку host_key_checking = False

2. Запускаем Vagrant up -- создаются ВМ, копируются ключи для пользователя vagrant, генерируется vagrant_ansible_inventory

3. Создается файл .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory его используем для отладки скриптов Ansible

4. запуск скриптов командой: ansible-playbook -v -i .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory services/alfresco_full_install.yml


## Примечание:

В версии 2.1 обнаружена ошибка https://github.com/ansible/ansible/issues/16060
В связи с этим скрипт services/haproxy.yml проходить полностью не будет.
После выполнения с ошибкой нужно подключиться по ssh к web1 и web2, и выполнить 'sudo service tomcat7 start'.